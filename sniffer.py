import socket
import scapy.all as scapy

def snf(interface):
    scapy.sniff(iface=interface, store=False, prn=process_sniffed_packet)

def process_sniffed_packet(packet):
    # здесь обработка перехваченного пакета
    print(packet)  # вывод пакета

snf("eno1")  #  Ethernet